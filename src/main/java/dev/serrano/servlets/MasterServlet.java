package dev.serrano.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dev.serrano.Controllers.EmployeeController;
import dev.serrano.Controllers.ManagerController;

/**
 * Servlet implementation class MasterServlet
 */
public class MasterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MasterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String uri = request.getRequestURI();
		//System.out.println(Paths.get(".").toAbsolutePath().normalize().toString());
		
		switch(uri) {
		case "/Project1WebFinal/getAllManagers.do":{ManagerController.getAllManagers(request, response);} break;
		case "/Project1WebFinal/login.do":{ManagerController.login(request, response);} break;
		case "/Project1WebFinal/empSessInfo.do":{EmployeeController.getEmployee(request, response);} break;
		case "/Project1WebFinal/RequestsByEmployee.do":{EmployeeController.getEmployeeRequests(request, response);}break;
		case "/Project1WebFinal/manSessInfo.do":{ManagerController.getManager(request, response);}break;
		case "/Project1WebFinal/getAllRequests.do":{ManagerController.getAllRequests(request, response);}break;
		case "/Project1WebFinal/logout.do":{ManagerController.logout(request, response);}break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uri = request.getRequestURI();
		
		switch(uri) {
		case "/Project1WebFinal/page.do":{ManagerController.login(request, response);} break;
		case "/Project1WebFinal/sendNewRequest.do":{EmployeeController.createNewRequest(request, response);} break;
		case "/Project1WebFinal/managerForm.do":{ManagerController.processForm(request, response);}break;
		}
		
		}

}
