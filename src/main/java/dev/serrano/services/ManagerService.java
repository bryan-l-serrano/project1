package dev.serrano.services;

import java.util.Set;

import entities.Manager;

public interface ManagerService {
	
	Manager viewManager(int id);
	Set<Manager> viewAllManagers();
	Manager getManagerByUserName(String name);
	
	Manager loginManager(String username, String password);
}
