package dev.serrano.services;

import daos.EmployeeDAO;
import daos.EmployeeDAOjdbc;
import entities.Employee;
import entities.Request;

public class EmployeeServiceImpl implements EmployeeService{

	public static EmployeeDAO edao = EmployeeDAOjdbc.getEmployeeDAOjdbc();
	
	
	@Override
	public Employee loginEmployee(String username, String password) {
		Employee employee = edao.viewEmployeeByUsername(username);
		if(employee != null) {
			if((employee.getPassword()).equals(password)) {
				return employee;
			}
			else {
				return null;
			}
		}
		else {
			return null;
		}
	}
	
	public Employee viewEmployeebyUsername(String username) {
		Employee em = edao.viewEmployeeByUsername(username);
		return em;
	}

	@Override
	public Employee viewEmployeeByID(int id) {
		return edao.viewEmployee(id);
	}

	@Override
	public boolean updateEmployee(Employee employee) {
		edao.updateEmployee(employee);
		return true;
	}


}
