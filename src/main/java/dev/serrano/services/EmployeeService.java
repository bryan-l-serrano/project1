package dev.serrano.services;

import entities.Employee;

public interface EmployeeService {
	
	Employee loginEmployee(String username, String password);
	Employee viewEmployeebyUsername(String username);
	Employee viewEmployeeByID(int id);
	boolean updateEmployee(Employee employee);

}
