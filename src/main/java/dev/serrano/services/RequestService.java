package dev.serrano.services;

import java.util.Set;

import entities.Request;

public interface RequestService {
	
	Set<Request> getRequestsByID(int id);
	boolean createNewRequest(double amount, int id, String reason);
	Set<Request> getAllRequests();
	Request getRequestByRequestID(int id);
	boolean updateRequest(String status, int requestID, String comment);

}
