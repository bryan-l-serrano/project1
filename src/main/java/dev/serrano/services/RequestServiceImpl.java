package dev.serrano.services;

import java.util.Set;

import daos.RequestDAO;
import daos.RequestDAOjdbc;
import entities.Request;

public class RequestServiceImpl implements RequestService{
	
	RequestDAO rdao = RequestDAOjdbc.getRequestDAOjdbc();
	
	@Override
	public Set<Request> getRequestsByID(int id) {
		Set<Request> requests = rdao.getRequestsByEmployee(id);
		return requests;
		
	}

	@Override
	public boolean createNewRequest(double amount, int id, String reason) {
		Request r = new Request(0, id, amount, reason);
		if(rdao.createRequest(r)){
			return true;
		}
		return false;
	}

	@Override
	public Set<Request> getAllRequests() {
		Set<Request> requests = rdao.getAllRequests();
		return requests;
	}

	@Override
	public boolean updateRequest(String status, int requestID, String comment) {
		Request request = rdao.getRequestByID(requestID);
		request.setStatus(status);
		request.setComment(comment);
		request.setTimeProcessed(System.currentTimeMillis());
		rdao.updateRequest(request);
		return true;
	}

	@Override
	public Request getRequestByRequestID(int id) {
		return rdao.getRequestByID(id);
	}
	
}
