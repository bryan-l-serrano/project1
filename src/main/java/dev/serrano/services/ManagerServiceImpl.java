package dev.serrano.services;

import java.util.Set;

import daos.ManagerDAO;
import daos.ManagerDAOjdbc;
import entities.Manager;

public class ManagerServiceImpl implements ManagerService{
	
	public static ManagerDAO mdao = ManagerDAOjdbc.getManagerDAOjdbc();

	@Override
	public Manager viewManager(int id) {
		return mdao.viewManager(id);
	}

	@Override
	public Set<Manager> viewAllManagers() {
		return mdao.viewAllManagers();
	}

	@Override
	public Manager getManagerByUserName(String name) {
		return mdao.viewManagerByUserName(name);
	}

	@Override
	public Manager loginManager(String username, String password) {
		Manager manager = mdao.viewManagerByUserName(username);
		if(manager != null) {
			if((manager.getPassword()).equals(password)) {
				return manager;
			}
			else {
				return null;
			}
		}
		else {
			return null;
		}
	}
	
	

}
