
package dev.serrano.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionUtil {

		public static Connection createConnection(){
			try {
				Properties props = new Properties();
					//File file = new File(".");
					//for(String names : file.list()) System.out.println(names);
					//System.out.println(Paths.get(".").toAbsolutePath().normalize().toString());
					//FileInputStream in = new FileInputStream("../Documents/workspace-spring-tool-suite-4-4.3.2.RELEASE/Project1WebApp/src/main/resources/connection.properties")
					FileInputStream in = new FileInputStream("src/main/resources/connection.properties");
					try {
						Class.forName("org.mariadb.jdbc.Driver");
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					props.load(in);
					
					String conInfo = props.getProperty("conn");
					Connection conn = DriverManager.getConnection(conInfo);
					//System.out.println(conn);
					return conn;
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
			}

}


