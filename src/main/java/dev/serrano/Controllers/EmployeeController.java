package dev.serrano.Controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.interfaces.RSAKey;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import dev.serrano.services.EmployeeService;
import dev.serrano.services.EmployeeServiceImpl;
import dev.serrano.services.RequestService;
import dev.serrano.services.RequestServiceImpl;
import entities.Employee;
import entities.Manager;
import entities.Request;

public class EmployeeController {

	EmployeeService es = new EmployeeServiceImpl();
	static RequestService rs = new RequestServiceImpl();
	
	
	public static void getEmployee(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Employee employee = (Employee)request.getSession().getAttribute("employee");
		PrintWriter pw = response.getWriter();
		Gson gson = new Gson();
		String json = gson.toJson(employee);
		pw.append(json);
	}
	
	public static void getEmployeeRequests(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Employee employee = (Employee)request.getSession().getAttribute("employee");
		Set<Request> requests = rs.getRequestsByID(employee.getEmployeeID());
		PrintWriter pw = response.getWriter();
		Gson gson = new Gson();
		String json = gson.toJson(requests);
		pw.append(json);	
	}
	
	public static void createNewRequest(HttpServletRequest request, HttpServletResponse response) throws IOException{
		String amount = request.getParameter("amount");
		String reason = request.getParameter("reason");
		Employee employee = (Employee)request.getSession().getAttribute("employee");
		rs.createNewRequest(Double.parseDouble(amount), employee.getEmployeeID(), reason);
		RequestDispatcher rd = request.getRequestDispatcher("/employee.html");
		try {
			rd.forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	

}
