package dev.serrano.Controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import dev.serrano.services.EmployeeService;
import dev.serrano.services.EmployeeServiceImpl;
import dev.serrano.services.ManagerService;
import dev.serrano.services.ManagerServiceImpl;
import dev.serrano.services.RequestService;
import dev.serrano.services.RequestServiceImpl;
import entities.Employee;
import entities.Manager;
import entities.Request;

public class ManagerController {
	private static ManagerService ms = new ManagerServiceImpl();
	private static EmployeeService es = new EmployeeServiceImpl();
	private static RequestService rs = new RequestServiceImpl();
	
	
	public static void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession thisSession = request.getSession();
		thisSession.invalidate();
	}
	
	
	public static void processForm(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String status = request.getParameter("status");
		int r_id = 0;
		try {
			r_id = Integer.parseInt(request.getParameter("requestID"));
		}catch(NumberFormatException e) {
			RequestDispatcher rd = request.getRequestDispatcher("/manager.html");
			try {
				rd.forward(request, response);
			} catch (ServletException l) {
				// TODO Auto-generated catch block
				l.printStackTrace();
			}
		}
		String comment = request.getParameter("comment");
		
		
		if(status.equals("Deny")){
			status = "Denied";
		}
		else if(status.equals("Approve")) {
			status = "Approved";
			int empID = (rs.getRequestByRequestID(r_id)).getEmployeeID();
			Employee employee = es.viewEmployeeByID(empID);
			employee.setBalance(employee.getBalance() + (rs.getRequestByRequestID(r_id)).getAmount());
			es.updateEmployee(employee);
		}
		System.out.println("request ID: " + r_id);
		boolean worked = rs.updateRequest(status, r_id, comment);
		PrintWriter pw = response.getWriter();
		Gson gson = new Gson();
		String json = gson.toJson(worked);
		pw.append(json);
		RequestDispatcher rd = request.getRequestDispatcher("/manager.html");
		try {
			rd.forward(request, response);
		} catch (ServletException e) {
		}
	}
	
	public static void getAllManagers(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Set<Manager> managers = ms.viewAllManagers();
		//System.out.println(Paths.get(".").toAbsolutePath().normalize().toString());
		PrintWriter pw = response.getWriter();
		System.out.println(managers);
		Gson gson = new Gson();
		String json = gson.toJson(managers);
		pw.append(json);
		
	}
	
	public static void getManager(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Manager man = (Manager)request.getSession().getAttribute("manager");
		//System.out.println(man);
		PrintWriter pw = response.getWriter();
		Gson gson = new Gson();
		String json = gson.toJson(man);
		pw.append(json);
	}
	
	public static void getAllRequests(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Set<Request> requests = rs.getAllRequests();
		PrintWriter pw = response.getWriter();
		Gson gson = new Gson();
		String json = gson.toJson(requests);
		pw.append(json);	
	}
	
	public static void login(HttpServletRequest request, HttpServletResponse response) throws IOException{
		String username = request.getParameter("userName");
		String password = request.getParameter("passWord");
		//System.out.println(username);
		//System.out.println(password);
		
		Manager manager = ms.loginManager(username, password);
		if(manager != null) {
			System.out.println("Successful Login");
			//System.out.println(manager);
			HttpSession sess = request.getSession();
			sess.setAttribute("manager", manager);
			RequestDispatcher rd = request.getRequestDispatcher("/manager.html");
			try {
				rd.forward(request, response);
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//response.sendRedirect("/manager.html");
		}
		else {
			Employee employee = es.loginEmployee(username, password);
			if(employee != null) {
				System.out.println("Successful Login");
				
				HttpSession sess = request.getSession();
				sess.setAttribute("employee", employee);
				
				RequestDispatcher rd = request.getRequestDispatcher("/employee.html");
				try {
					rd.forward(request, response);
				} catch (ServletException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				RequestDispatcher rd = request.getRequestDispatcher("/login.html");
				try {
					rd.forward(request, response);
				} catch (ServletException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
		}

	}
}
