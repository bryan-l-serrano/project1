package entities;

public class Request {
	private int requestID;
	private int employeeID;
	private double amount;
	private String status;
	private String reason;
	private String comment;
	private long timeCreated;
	private long timeProcessed;
	
	public Request(){
		super();
	}
	
	public Request(int requestID, int employeeID, double amount, String reason) {
		super();
		this.requestID = requestID;
		this.employeeID = employeeID;
		this.amount = amount;
		this.status = "Submitted";
		this.reason = reason;
		this.comment = "";
		this.timeProcessed = 0;
		this.timeCreated = System.currentTimeMillis();
	}

	public int getRequestID() {
		return requestID;
	}

	public void setRequestID(int requestID) {
		this.requestID = requestID;
	}

	public int getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(long timeCreated) {
		this.timeCreated = timeCreated;
	}

	public long getTimeProcessed() {
		return timeProcessed;
	}

	public void setTimeProcessed(long timeProcessed) {
		this.timeProcessed = timeProcessed;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
	
}
