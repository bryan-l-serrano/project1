package entities;

public class Employee {
	private int EmployeeID;
	private String userName;
	private String firstName;
	private String lastName;
	private String Password;
	private double balance;
	private int managerID;
	
	
	public Employee() {
		super();
	}
	
	public Employee(int employeeID, String firstName, String lastName, double balance, int managerID) {
		super();
		EmployeeID = employeeID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.balance = balance;
		this.managerID = managerID;
	}
	
	
	public int getEmployeeID() {
		return EmployeeID;
	}
	public void setEmployeeID(int employeeID) {
		EmployeeID = employeeID;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public int getManagerID() {
		return managerID;
	}
	public void setManagerID(int managerID) {
		this.managerID = managerID;
	}

	@Override
	public String toString() {
		return "Employee [EmployeeID=" + EmployeeID + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", balance=" + balance + ", managerID=" + managerID + "]";
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	
	

}
