package daos;

import java.util.Set;

import entities.Manager;

public interface ManagerDAO {
	
	//create
	Manager createManager(Manager manager);
	
	//read
	Manager viewManager(int id);
	Set<Manager> viewAllManagers();
	Manager viewManagerByUserName(String name);
	
	
	//update
	Manager updateManager(Manager manager);
	
	//delete
	Manager deleateManager(Manager manager);
}
