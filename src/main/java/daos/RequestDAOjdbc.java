package daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import dev.serrano.utils.ConnectionUtil;
import entities.Request;

public class RequestDAOjdbc implements RequestDAO{

	private static RequestDAOjdbc rdao;
	
	private RequestDAOjdbc() {
	}
	
	public static RequestDAO getRequestDAOjdbc() {
		if(rdao==null) {
			rdao = new RequestDAOjdbc();
		}
		return rdao;
	}
	
	@Override
	public boolean createRequest(Request request) {
		try(Connection conn = ConnectionUtil.createConnection()){
			String sql = "INSERT INTO Project1.Request VALUES(0,?,?,?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, request.getEmployeeID());
			ps.setDouble(2, request.getAmount());
			ps.setString(3, request.getStatus());
			ps.setString(4, request.getReason());
			ps.setString(5, request.getComment());
			ps.setLong(6, request.getTimeCreated());
			ps.setLong(7, request.getTimeProcessed());
			ps.execute();
			return true;
			
		}catch(SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Request getRequest(Request request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Request> getAllRequests() {
		try(Connection conn = ConnectionUtil.createConnection()){
			Set<Request> requests = new HashSet<Request>();
			String sql = "SELECT * FROM Project1.Request";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				Request r = new Request();
				r.setRequestID(rs.getInt("r_id"));
				r.setEmployeeID(rs.getInt("employeeID"));
				r.setAmount(rs.getDouble("amount"));
				r.setStatus(rs.getString("status"));
				r.setReason(rs.getString("reason"));
				r.setComment(rs.getString("comment"));
				r.setTimeCreated(rs.getLong("timeCreated"));
				r.setTimeProcessed(rs.getLong("timeProcessed"));
				requests.add(r);
			}
			
			return requests;
		}
		catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Set<Request> getRequestsByEmployee(int id) {
		try(Connection conn = ConnectionUtil.createConnection()){
			Set<Request> requests = new HashSet<Request>();
			String sql = "SELECT * FROM Project1.Request WHERE employeeID = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				Request r = new Request();
				r.setRequestID(rs.getInt("r_id"));
				r.setEmployeeID(rs.getInt("employeeID"));
				r.setAmount(rs.getDouble("amount"));
				r.setStatus(rs.getString("status"));
				r.setReason(rs.getString("reason"));
				r.setComment(rs.getString("comment"));
				r.setTimeCreated(rs.getLong("timeCreated"));
				r.setTimeProcessed(rs.getLong("timeProcessed"));
				System.out.println(r);
				requests.add(r);
			}
			
			return requests;
		}
		catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Set<Request> getRequestsByManager(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Request> getRequestsByStatus(String Status) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Request updateRequest(Request request) {
		try(Connection conn = ConnectionUtil.createConnection()){
			String sql = "UPDATE Project1.Request SET r_id = ?, employeeID  = ?, amount = ?, status = ?, reason = ?, comment = ?, timeCreated = ?, timeProcessed = ? WHERE r_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setInt(1, request.getRequestID());
			ps.setInt(2, request.getEmployeeID());
			ps.setDouble(3, request.getAmount());
			ps.setString(4, request.getStatus());
			ps.setString(5, request.getReason());
			ps.setString(6,  request.getComment());
			ps.setLong(7, request.getTimeCreated());
			ps.setLong(8, request.getTimeProcessed());
			ps.setInt(9,  request.getRequestID());
			ps.execute();
			return request;
			
		}
		catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Request deleteRequest(Request request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Request getRequestByID(int id) {
		try(Connection conn = ConnectionUtil.createConnection()){
			String sql = "SELECT * FROM Project1.Request WHERE r_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			rs.next();
			Request r = new Request();
			r.setRequestID(rs.getInt("r_id"));
			r.setEmployeeID(rs.getInt("employeeID"));
			r.setAmount(rs.getDouble("amount"));
			r.setStatus(rs.getString("status"));
			r.setReason(rs.getString("reason"));
			r.setComment(rs.getString("comment"));
			r.setTimeCreated(rs.getLong("timeCreated"));
			r.setTimeProcessed(rs.getLong("timeProcessed"));
			
			return r;
		}
		catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
