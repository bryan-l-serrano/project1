package daos;

import java.util.Set;

import entities.Employee;
import entities.Manager;

public interface EmployeeDAO {
	
	//create
	
	Employee createEmployee(Employee employee);
	
	//read
	Employee viewEmployee(int id);
	Employee viewEmployeeByUsername(String username);
	Set<Employee> viewAllEmployees();
	Set<Employee> viewAllEmployeesByManager(Manager manager);
	
	//update
	Employee updateEmployee(Employee employee);
	
	//delete
	Employee deleteEmployee(Employee employee);
}
