package daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import dev.serrano.utils.ConnectionUtil;
import entities.Manager;

public class ManagerDAOjdbc implements ManagerDAO{
	
	private static ManagerDAOjdbc mdao;
	
	private ManagerDAOjdbc() {};
	
	public static ManagerDAO getManagerDAOjdbc() {
		if(mdao==null) {
			mdao = new ManagerDAOjdbc();
		}
		return mdao;
	}

	public Manager createManager(Manager manager) {
		//not necessary - unimplemented
		return null;
	}

	public Manager viewManager(int id) {
		
		try(Connection conn = ConnectionUtil.createConnection()){
			String sql = "Select * FROM Project1.Manager WHERE m_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1,  id);
			
			ResultSet rs = ps.executeQuery();
			
			rs.next();
			
			Manager manager = new Manager();
			manager.setId(rs.getInt("m_id"));
			manager.setFirstName(rs.getString("fname"));
			manager.setLastName(rs.getString("lname"));
			manager.setUserName(rs.getString("userName"));
			manager.setPassword(rs.getString("password"));
			
			return manager;
			
			
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Set<Manager> viewAllManagers() {
		
		try(Connection conn = ConnectionUtil.createConnection()){
			System.out.println(conn);
			Set<Manager> managers = new HashSet<Manager>();
			
			String sql = "SELECT * FROM Project1.Manager";
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
			
				Manager manager = new Manager();
				manager.setId(rs.getInt("m_id"));
				manager.setFirstName(rs.getString("fname"));
				manager.setLastName(rs.getString("lname"));
				manager.setUserName(rs.getString("userName"));
				manager.setPassword(rs.getString("password"));
				managers.add(manager);
			}
			System.out.println(managers);
			return managers;
			
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Manager updateManager(Manager manager) {
		//Unimplimented
		//not necessary for this project
		return null;
	}

	public Manager deleateManager(Manager manager) {
		// Unimpliminted
		// not necessary for this project
		return null;
	}

	@Override
	public Manager viewManagerByUserName(String name) {
		try(Connection conn = ConnectionUtil.createConnection()){
			String sql = "Select * FROM Project1.Manager WHERE userName = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1,  name);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				Manager manager = new Manager();
				manager.setId(rs.getInt("m_id"));
				manager.setFirstName(rs.getString("fname"));
				manager.setLastName(rs.getString("lname"));
				manager.setUserName(rs.getString("userName"));
				manager.setPassword(rs.getString("password"));
				System.out.println(manager.getFirstName());
				return manager;
			}
			else {
				//System.out.println("your dao isn't working");
				return null;
			}
			
			
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

}
