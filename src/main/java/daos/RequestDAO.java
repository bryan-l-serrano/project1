package daos;

import java.util.Set;

import entities.Request;

public interface RequestDAO {
	//create
	boolean createRequest(Request request);
	//read
	Request getRequest(Request request);
	Request getRequestByID(int id);
	Set<Request> getAllRequests();
	Set<Request> getRequestsByEmployee(int id);
	Set<Request> getRequestsByManager(int id);
	Set<Request> getRequestsByStatus(String Status);
	//update
	
	Request updateRequest(Request request);
	//delete
	Request deleteRequest(Request request);
}
