package daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import dev.serrano.utils.ConnectionUtil;
import entities.Employee;
import entities.Manager;

public class EmployeeDAOjdbc implements EmployeeDAO{
	
	private static EmployeeDAO edao;
	
	private EmployeeDAOjdbc() {};
	
	public static EmployeeDAO getEmployeeDAOjdbc() {
		if(edao==null) {
			edao = new EmployeeDAOjdbc();
		}
		return edao;
	}

	@Override
	public Employee createEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee viewEmployee(int id) {
			try(Connection conn = ConnectionUtil.createConnection()){
			
			String sql = "SELECT * FROM Project1.Employee where e_id = ?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			
			ResultSet rs = ps.executeQuery();
			
			rs.next();
			
			Employee employee = new Employee();
			
			employee.setEmployeeID(rs.getInt("e_id"));
			employee.setUserName(rs.getString("userName"));
			employee.setPassword(rs.getString("password"));
			employee.setLastName(rs.getString("lName"));
			employee.setFirstName(rs.getString("fName"));
			employee.setBalance(rs.getDouble("balance"));
			employee.setManagerID(rs.getInt("managerID"));
			return employee;
			
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Set<Employee> viewAllEmployees() {
		try(Connection conn = ConnectionUtil.createConnection()){
			
			String sql = "SELECT * FROM Project1.Employee";
			Set<Employee> employees = new HashSet<Employee>();
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
			
			Employee employee = new Employee();
			
			employee.setEmployeeID(rs.getInt("e_id"));
			employee.setUserName(rs.getString("userName"));
			employee.setPassword(rs.getString("password"));
			employee.setLastName(rs.getString("lName"));
			employee.setFirstName(rs.getString("fName"));
			employee.setBalance(rs.getDouble("balance"));
			employee.setManagerID(rs.getInt("managerID"));
			employees.add(employee);
			}
			
			return employees;
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Set<Employee> viewAllEmployeesByManager(Manager manager) {
			try(Connection conn = ConnectionUtil.createConnection()){
			
			String sql = "SELECT * FROM Project1.Employee WHERE managerID = ?";
			Set<Employee> employees = new HashSet<Employee>();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, manager.getId());
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
			
			Employee employee = new Employee();
			
			employee.setEmployeeID(rs.getInt("e_id"));
			employee.setUserName(rs.getString("userName"));
			employee.setPassword(rs.getString("password"));
			employee.setLastName(rs.getString("lName"));
			employee.setFirstName(rs.getString("fName"));
			employee.setBalance(rs.getDouble("balance"));
			employee.setManagerID(rs.getInt("managerID"));
			employees.add(employee);
			}
			
			return employees;
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		try(Connection conn = ConnectionUtil.createConnection()){
			String sql = "UPDATE Project1.Employee SET e_id = ?, userName = ?, fName = ?, lName = ?, password = ?, balance = ?, managerID = ? WHERE e_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setInt(1, employee.getEmployeeID());
			ps.setString(2, employee.getUserName());
			ps.setString(3, employee.getFirstName());
			ps.setString(4, employee.getLastName());
			ps.setString(5, employee.getPassword());
			ps.setDouble(6, employee.getBalance());
			ps.setInt(7, employee.getManagerID());
			ps.setInt(8,employee.getEmployeeID());
			ps.execute();
			return employee;
			
		}
		catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Employee deleteEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee viewEmployeeByUsername(String username) {
try(Connection conn = ConnectionUtil.createConnection()){
			
			String sql = "SELECT * FROM Project1.Employee WHERE userName = ?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
			
			Employee employee = new Employee();
			
			employee.setEmployeeID(rs.getInt("e_id"));
			employee.setUserName(rs.getString("userName"));
			employee.setPassword(rs.getString("password"));
			employee.setLastName(rs.getString("lName"));
			employee.setFirstName(rs.getString("fName"));
			employee.setBalance(rs.getDouble("balance"));
			employee.setManagerID(rs.getInt("managerID"));
			return employee;
			}
			else {
				return null;
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

}
